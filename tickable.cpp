//
// Base class for tickable objects.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "tickable.h"


//--------------------------------------------------------------------------

Tickable::Tickable() :
   LocalTimeObject()
{
   
}

//--------------------------------------------------------------------------

void Tickable::tick()
{
   syncLocalTime();
         
   onTick();
}
