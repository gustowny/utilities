//
// Base class for tickable objects.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "localtimeobject.h"


//--------------------------------------------------------------------------

LocalTimeObject::LocalTimeObject() :
   m_localTime(0),
   m_localTimeOffset(0),
   m_lastSystemTime(0)
{
   
}

//--------------------------------------------------------------------------

void LocalTimeObject::syncLocalTime()
{
   const unsigned long systemTime = millis();
   
   m_localTime      = systemTime - m_localTimeOffset; // will handle systemtime overflow
   m_lastSystemTime = systemTime;
}

//--------------------------------------------------------------------------

unsigned long LocalTimeObject::localTime()
{ 
   return m_localTime; 
}


//--------------------------------------------------------------------------

void LocalTimeObject::resetLocalTime()
{  
   m_localTime       = 0;
   m_localTimeOffset = m_lastSystemTime;
}