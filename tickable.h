//
// Abstract base class for tickable objects.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _TICKABLE_H_
#define _TICKABLE_H_

#include "arduino.h"
#include "localtimeobject.h"

class Tickable : public LocalTimeObject
{
   public:
   
      // Constructor.
      Tickable();
      
      // Updates the local time and then calls onTick().
      void tick();
   
   protected:
   
      // Shall contain the code that shall be executed at each tick.
      virtual void onTick() = 0;
      
   private:
   
   
};

#endif // _TICKABLE_H_