//
// Abstract base class for objects that need a local object time.
// The local object time can be reset (when safe for the object)
// to avoid time overflow.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _LOCALTIMEOBJECT_H_
#define _LOCALTIMEOBJECT_H_

#include "arduino.h"

class LocalTimeObject
{
   public:
   
      // Constructor.
      LocalTimeObject();
        
   protected:
   
      // Synchronizes the local object time with the system time.
      virtual void syncLocalTime();
      
      // Returns the local object time in milliseconds. Is updated
      // by syncLocalTime() and is constant after that.
      virtual unsigned long localTime();
      
      // Resets the local object time to zero. Could be used to avoid
      // overflow of local object time.
      virtual void resetLocalTime();
   
   private:
   
      unsigned long  m_localTime;
      unsigned long  m_localTimeOffset;
      unsigned long  m_lastSystemTime;
   
};

#endif // _LOCALTIMEOBJECT_H_